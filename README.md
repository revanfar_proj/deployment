# vCPE Deployment script were made using YAML language.
In order to use it, you must atleast install ansible on your own machine and NOT ON THE VCPE.

Installing Ansible:
	1. The best way to get Ansible for Ubuntu is to add the project's PPA (personal package archive) to your system. We can add the Ansible PPA by typing the following command:
	sudo apt-add-repository ppa:ansible/ansible
	2. Next, we need to refresh our system's package index so that it is aware of the packages available in the PPA. Afterwards, we can install the software:
	sudo apt-get update
	sudo apt-get install ansible

Things to do before deploying on vCPE:
	1. Make sure that vCPE has ubuntu server 16.04.2 LTS OS
	2. vCPE's openssh-server is running on port 22 
	3. Create a private and public key by using the following command: ssh-keygen 
	4. Upload your public key into vCPE using the following command : ssh-copy-id revan@x.x.x.x
	5. Now try to ssh vCPE and make sure that you don't need any password to remote it.